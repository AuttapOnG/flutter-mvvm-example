// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_article_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsArticleRequest _$NewsArticleRequestFromJson(Map<String, dynamic> json) {
  return NewsArticleRequest(
    json['country'] as String,
  )..apiKey = json['apiKey'] as String;
}

Map<String, dynamic> _$NewsArticleRequestToJson(NewsArticleRequest instance) =>
    <String, dynamic>{
      'country': instance.country,
      'apiKey': instance.apiKey,
    };
