import 'package:json_annotation/json_annotation.dart';

part 'news_article_request.g.dart';

@JsonSerializable()
class NewsArticleRequest {
  String country;
  String apiKey = "47f643b3959040dd853d31ac081cb886";

  NewsArticleRequest(this.country);

  Map<String, dynamic> toJson() => _$NewsArticleRequestToJson(this);
}
