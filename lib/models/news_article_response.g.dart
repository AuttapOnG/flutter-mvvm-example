// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_article_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsArticleResponse _$NewsArticleResponseFromJson(Map<String, dynamic> json) {
  return NewsArticleResponse(
    json['status'] as String,
    (json['articles'] as List<dynamic>)
        .map((e) => NewsArticle.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NewsArticleResponseToJson(
        NewsArticleResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'articles': instance.articles,
    };
