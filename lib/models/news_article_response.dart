import 'package:json_annotation/json_annotation.dart';
import 'package:mvvm_example/models/news_article.dart';

import 'base_response.dart';

part 'news_article_response.g.dart';

@JsonSerializable()
class NewsArticleResponse extends BaseResponse {
  List<NewsArticle> articles;

  NewsArticleResponse(String status, this.articles) : super(status);

  factory NewsArticleResponse.fromJson(Map<String, dynamic> json) =>
      _$NewsArticleResponseFromJson(json);
}
