import 'package:flutter/material.dart';
import 'package:mvvm_example/screens/news_screen.dart';
import 'package:mvvm_example/viewmodels/news_article_list_view_model.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => NewsArticleListViewModel())
        ],
        child: NewsScreen(),
      ),
    );
  }
}
