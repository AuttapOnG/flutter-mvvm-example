import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mvvm_example/api/api_client.dart';
import 'package:mvvm_example/models/news_article.dart';
import 'package:mvvm_example/models/news_article_request.dart';
import 'package:mvvm_example/models/news_article_response.dart';
import 'package:mvvm_example/viewmodels/news_article_view_model.dart';

enum LoadingStatus { completed, searching, empty }

class NewsArticleListViewModel with ChangeNotifier {
  LoadingStatus loadingStatus = LoadingStatus.empty;
  List<NewsArticleViewModel> articles = <NewsArticleViewModel>[];

  void topHeadlines() async {
    final client = ApiClient(Dio());
    NewsArticleRequest request = NewsArticleRequest("th");
    NewsArticleResponse response =
        await client.getTopHeadlines(request.toJson());
    List<NewsArticle> newsArticles = response.articles;
    this.articles =
        newsArticles.map((e) => NewsArticleViewModel(article: e)).toList();
    notifyListeners();

    if (this.articles.isEmpty) {
      this.loadingStatus = LoadingStatus.empty;
    } else {
      this.loadingStatus = LoadingStatus.completed;
    }
    notifyListeners();
  }
}
