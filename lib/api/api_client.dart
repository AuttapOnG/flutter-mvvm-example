import 'package:dio/dio.dart';
import 'package:mvvm_example/models/news_article_response.dart';
import 'package:retrofit/retrofit.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: "https://newsapi.org/v2/")
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @GET("/top-headlines")
  Future<NewsArticleResponse> getTopHeadlines(
      @Queries() Map<String, dynamic> parameter);
}
